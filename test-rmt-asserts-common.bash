#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common-test.
#
# rmt-common-test is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common-test is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common-test. If not, see <https://www.gnu.org/licenses/>. 

###########
# TODO
###########

#
# - convert into proper bash asserts
#   => see also bash-asserts project
#

###########
# ASSERTS
###########

assert-lines-equal() {
  declare -n linesRef="$1"
  
  local lineExpected 
  local counterExpected=0 numGivenLines=${#linesRef[*]}

  while read -r lineExpected; do
    [[ ${counterExpected} -ge ${numGivenLines} ]] && {
      printf '# number of given lines [%s] exceeds number of expected lines [%s]\n' "$((counterExpected+1))" "${numGivenLines}" >&3
      return 1
    }
    [[ "${linesRef[${counterExpected}]}" != "${lineExpected}" ]] && {
      printf '# given line [%s][%s] is not expected line [%s]\n' "${counterExpected}" "${linesRef[${counterExpected}]}" "${lineExpected}" >&3
      return 1
    }
    counterExpected=$((counterExpected+1))
  done

  [[ ${counterExpected} -gt ${numGivenLines} ]] && {
    printf '# number of given lines [%s] is larger than number of expected lines [%s]\n' "${counterExpected}" "${numGivenLines}" >&3
    return 1
  }
  return 0
}

assert-lists-equal() {
  declare -n listRef="$2"
  assert-lines-equal "$1" < <(printf '%s\n' "${listRef[@]}")
}
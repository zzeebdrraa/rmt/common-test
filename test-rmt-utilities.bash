#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common-test.
#
# rmt-common-test is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common-test is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common-test. If not, see <https://www.gnu.org/licenses/>.

############################
# UTILITIES
############################

read-file-buffer() {
  local -r fileBufferPath="$1"
  declare -r -n  outListRef="$2"
  
  [[ ! -f "${fileBufferPath}" ]] && return 1
  
  local line
  while read -r line; do
    outListRef+=("${line}")
  done <"${fileBufferPath}"

  return 0
}
# rmt-common-test

A small library of common functionality for bats unit testing.

## Installation

To Be Done

## Usage

To Be Done

## ToDo

- adopt asserts to be usable in bats
- adopt mocks to be usable in bats
- install via make

## License

[GPL3](https://www.gnu.org/licenses/gpl-3.0.html#license-text)

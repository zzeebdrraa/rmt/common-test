#!/usr/bin/env bash

# Copyright (C) 2023 zeebdrraa
#
# This file is part of rmt-common-test.
#
# rmt-common-test is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or 
# (at your option) any later version.
#
# rmt-common-test is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with rmt-common-test. If not, see <https://www.gnu.org/licenses/>. 

###########
# TODO
###########

#
# - convert into proper bash mocks
#   => see also bash-mocks project
#

############################
# GLOBAL DATA
############################
# can be overwritten. also, a path can be added, as long as path exists

declare -g __test_file_buffer='test-mock-file_buffer'

###########
# Mocks
###########

mock-cmd-error() {
  return 1
}

mock-cmd-error-with-message() {
  printf 'a command error message\n' >&2
  return 1
}

mock-cmd-ok-no-output() {
  return 0
}

mock-cmd-ok-print-args() {
  printf '%s\n' "$*"
  return 0
}

mock-cmd-ok-append-args-to-file() {
  printf '%s\n' "$@" >>"${__test_file_buffer}"
  return 0
}